<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SqlModel extends CI_Model{
    
    public function insertValue($table, $data){
        return $this->db->insert($table, $data);
    }

    public function updateValue($table, $where, $data){
        $this->db->where($where);
        return $this->db->update($table, $data);
    }

    public function deleteValue($table, $where){
        return $this->db->delete($table, $where);
    }

    public function selectAll($table, $where=array()){
        $this->db->select('*')
        ->from($table)
        ->where($where);

        $query = $this->db->get();         
        return $query->result();

    }

    public function selectOne($table, $where=array()){
        $this->db->select('*')
        ->from($table)
        ->where($where);

        $query = $this->db->get();         
        return $query->row();
    }

}
?>