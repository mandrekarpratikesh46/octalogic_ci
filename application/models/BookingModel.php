<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BookingModel extends CI_Model{
    
    public function checkIfBookingExist($vehicle, $from, $to){
        $this->db->select('*')
        ->from("booking")
        ->where("(('$from' BETWEEN dFrom and dTo) OR ('$to' BETWEEN dFrom and dTo) OR (dFrom BETWEEN '$from' and '$to') OR (dTo BETWEEN '$from' and '$to')) and iVehicleID = '$vehicle' and cStatus != 'X'"); // where cStatus X meaning deleted booking
        $query = $this->db->get();         
        return $query->result();
    }

    public function createBooking($data){
        
        // unlocking table
        $this->db->query('LOCK TABLE booking WRITE');

        // getting next ID for booking
        $this->db->select('max(iBookingID) as nextid')
        ->from('booking');
        $query = $this->db->get();         
        $bookingID = $query->row()->nextid;

        if(empty($bookingID)){
            $bookingID = 1;
        }else{
            $bookingID += 1;
        }

        // generating booking code
        $bookingCode = "OTLC";
        $bookingCode.= $bookingID.'-';
        $bookingCode .= rand(11111, 99999);

        // adding data
        $data["iBookingID"] = $bookingID;
        $data["vCode"] = $bookingCode;  

        // inserting data
        $result = $this->db->insert("booking", $data);

        // unlocking table
        $this->db->query('UNLOCK TABLES');

        return $result;
    }
}
?>