<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/userguide3/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

// routes for wheels
$route['api/wheels'] = 'api/WheelController';
$route['api/wheels/create'] = 'api/WheelController/createWheel';
$route['api/wheels/(:any)'] = 'api/WheelController/$1';
$route['api/wheels/update/(:any)'] = 'api/WheelController/updateWheel/$1';
$route['api/wheels/delete/(:any)'] = 'api/WheelController/deleteWheel/$1';

// routes for vehicle type
$route['api/types'] = 'api/TypeController';
$route['api/types/create'] = 'api/TypeController/createType';
$route['api/types/(:any)'] = 'api/TypeController/$1';
$route['api/types/update/(:any)'] = 'api/TypeController/updateType/$1';
$route['api/types/delete/(:any)'] = 'api/TypeController/deleteType/$1';

// routes for vehicle models
$route['api/models'] = 'api/ModelController';
$route['api/models/create'] = 'api/ModelController/createModel';
$route['api/models/(:any)'] = 'api/ModelController/$1';
$route['api/models/update/(:any)'] = 'api/ModelController/updateModel/$1';
$route['api/models/delete/(:any)'] = 'api/ModelController/deleteModel/$1';

// routes for bookings
$route['api/bookings'] = 'api/BookingController';
$route['api/bookings/create'] = 'api/BookingController/createBooking';
$route['api/bookings/(:any)'] = 'api/BookingController/$1';