<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/RestController.php';
require APPPATH . 'libraries/Format.php';
use chriskacerguis\RestServer\RestController;

class WheelController extends RestController
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('SqlModel');
    }

    public function index_get($id=0)
    {
        if(empty($id)){
            $SqlModel = new SqlModel;
            $wheels = $SqlModel->selectAll("wheels");
            $response = array(
                "statusCode"=> RestController::HTTP_OK,
                "message"=> "Wheels fetched successfully",
                "data"=> $wheels
            );
        }else{
            $SqlModel = new SqlModel;
            $cond = array("iWheelsID"=>$id);
            $wheels = $SqlModel->selectOne("wheels", $cond);
            $response = array(
                "statusCode"=> RestController::HTTP_OK,
                "message"=> "Wheel fetched successfully",
                "data"=> $wheels
            );
        }
        $this->response($response);
    }

    public function createWheel_post()
    {
        $SqlModel = new SqlModel;

        // getting form data
        $name = $this->input->post('name');
        $status = $this->input->post('status');

        if(empty($name) || empty($status)){
            $err = ''; // defining error
            empty($name)? $err = "Name cannot be empty" : '';
            empty($status)? $err = "Status cannot be empty" : '';

            $response = array(
                "statusCode"=> RestController::HTTP_BAD_REQUEST,
                "error"=> $err
            );
        }else{
            $data = array(
                'vName' => $name,
                'cStatus' => $status
            );
            $result = $SqlModel->insertValue("wheels", $data);
            if($result > 0){
                $response = array(
                    "statusCode"=> RestController::HTTP_OK,
                    "message"=> "Wheel created successfully"
                ); 
            }
            else{
                $response = array(
                    "statusCode"=> RestController::HTTP_BAD_REQUEST,
                    "message"=> "Something went wrong creating wheels"
                );
            }
        }
        $this->response($response);
    }

    public function updateWheel_post($id)
    {
        $SqlModel = new SqlModel;

        // getting form data
        $name = $this->input->post('name');
        $status = $this->input->post('status');

        if(empty($name) || empty($status) || empty($id)){
            $err = ''; // defining error
            empty($name)? $err = "Name cannot be empty" : '';
            empty($status)? $err = "Status cannot be empty" : '';
            empty($id)? $err = "ID cannot be empty" : '';

            $response = array(
                "statusCode"=> RestController::HTTP_BAD_REQUEST,
                "error"=> $err
            );
        }else{
            $data = array(
                'vName' => $name,
                'cStatus' => $status
            );
            $cond = array("iWheelsID"=>$id);
            $result = $SqlModel->updateValue("wheels", $cond, $data);
            if($result > 0){
                $response = array(
                    "statusCode"=> RestController::HTTP_OK,
                    "message"=> "wheels updated successfully"
                ); 
            }
            else{
                $response = array(
                    "statusCode"=> RestController::HTTP_BAD_REQUEST,
                    "message"=> "Something went wrong updating wheels"
                );
            }
        }
        $this->response($response);
    }

    public function deleteWheel_delete($id)
    {
        $SqlModel = new SqlModel;
        if(!empty($id)){
            $cond = array("iWheelsID"=>$id);
            $result = $SqlModel->deleteValue("wheels", $cond);
            if($result > 0){
                $response = array(
                    "statusCode"=> RestController::HTTP_OK,
                    "message"=> "wheels deleted successfully"
                );
            }
            else{
                $response = array(
                    "statusCode"=> RestController::HTTP_BAD_REQUEST,
                    "error"=> "Something went wrong deleting wheels"
                );
            }
        }else{
            $response = array(
                "statusCode"=> RestController::HTTP_BAD_REQUEST,
                "error"=> "ID cannot be empty"
            );
        }
        $this->response($response);
    }
}

?>