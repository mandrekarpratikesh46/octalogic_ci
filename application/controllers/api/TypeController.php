<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/RestController.php';
require APPPATH . 'libraries/Format.php';
use chriskacerguis\RestServer\RestController;

class TypeController extends RestController
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('SqlModel');
    }

    public function index_get($id=0)
    {
        if(empty($id)){
            $SqlModel = new SqlModel;
            $types = $SqlModel->selectAll("vehicle_type");
            $response = array(
                "statusCode"=> RestController::HTTP_OK,
                "message"=> "Vehicle types fetched successfully",
                "data"=> $types
            );
        }else{
            $SqlModel = new SqlModel;
            $cond = array("iVTypeID"=>$id);
            $types = $SqlModel->selectOne("vehicle_type", $cond);
            $response = array(
                "statusCode"=> RestController::HTTP_OK,
                "message"=> "Vehicle type fetched successfully",
                "data"=> $types
            );
        }
        $this->response($response);
    }

    public function createType_post()
    {
        $SqlModel = new SqlModel;

        // getting form data
        $name = $this->input->post('name');
        $status = $this->input->post('status');
        $wheels = $this->input->post('wheels');

        if(empty($name) || empty($status) || empty($wheels)){
            $err = ''; // defining error
            empty($name)? $err = "Name cannot be empty" : '';
            empty($status)? $err = "Status cannot be empty" : '';
            empty($wheels)? $err = "Wheels cannot be empty" : '';

            $response = array(
                "statusCode"=> RestController::HTTP_BAD_REQUEST,
                "error"=> $err
            );
        }else{
            $data = array(
                'iWheelsID'=> $wheels,
                'vName' => $name,
                'cStatus' => $status
            );
            $result = $SqlModel->insertValue("vehicle_type", $data);
            if($result > 0){
                $response = array(
                    "statusCode"=> RestController::HTTP_OK,
                    "message"=> "Vehicle type created successfully"
                ); 
            }
            else{
                $response = array(
                    "statusCode"=> RestController::HTTP_BAD_REQUEST,
                    "message"=> "Something went wrong creating vehicle type"
                );
            }
        }
        $this->response($response);
    }

    public function updateType_post($id)
    {
        $SqlModel = new SqlModel;

        // getting form data
        $name = $this->input->post('name');
        $status = $this->input->post('status');
        $wheels = $this->input->post('wheels');

        if(empty($name) || empty($status) || empty($wheels) || empty($id)){
            $err = ''; // defining error
            empty($name)? $err = "Name cannot be empty" : '';
            empty($status)? $err = "Status cannot be empty" : '';
            empty($wheels)? $err = "Wheels cannot be empty" : '';
            empty($id)? $err = "ID cannot be empty" : '';

            $response = array(
                "statusCode"=> RestController::HTTP_BAD_REQUEST,
                "error"=> $err
            );
        }else{
            $data = array(
                'iWheelsID'=> $wheels,
                'vName' => $name,
                'cStatus' => $status
            );
            $cond = array("iVTypeID"=>$id);
            $result = $SqlModel->updateValue("vehicle_type", $cond, $data);
            if($result > 0){
                $response = array(
                    "statusCode"=> RestController::HTTP_OK,
                    "message"=> "Vehicle type updated successfully"
                ); 
            }
            else{
                $response = array(
                    "statusCode"=> RestController::HTTP_BAD_REQUEST,
                    "message"=> "Something went wrong updating vehicle type"
                );
            }
        }
        $this->response($response);
    }

    public function deleteType_delete($id)
    {
        $SqlModel = new SqlModel;
        if(!empty($id)){
            $cond = array("iVTypeID"=>$id);
            $result = $SqlModel->deleteValue("vehicle_type", $cond);
            if($result > 0){
                $response = array(
                    "statusCode"=> RestController::HTTP_OK,
                    "message"=> "Vehicle type deleted successfully"
                );
            }
            else{
                $response = array(
                    "statusCode"=> RestController::HTTP_BAD_REQUEST,
                    "error"=> "Something went wrong deleting vehicle type"
                );
            }
        }else{
            $response = array(
                "statusCode"=> RestController::HTTP_BAD_REQUEST,
                "error"=> "ID cannot be empty"
            );
        }
        $this->response($response);
    }
}

?>