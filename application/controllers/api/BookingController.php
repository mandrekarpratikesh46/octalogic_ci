<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/RestController.php';
require APPPATH . 'libraries/Format.php';
use chriskacerguis\RestServer\RestController;

class BookingController extends RestController
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('SqlModel');
        $this->load->model('BookingModel');
    }

    public function index_get($id=0)
    {
        if(empty($id)){
            $SqlModel = new SqlModel;
            $types = $SqlModel->selectAll("booking");
            $response = array(
                "statusCode"=> RestController::HTTP_OK,
                "message"=> "Bookings fetched successfully",
                "data"=> $types
            );
        }else{
            $SqlModel = new SqlModel;
            $cond = array("iBookingID"=>$id);
            $types = $SqlModel->selectOne("booking", $cond);
            $response = array(
                "statusCode"=> RestController::HTTP_OK,
                "message"=> "Booking fetched successfully",
                "data"=> $types
            );
        }
        $this->response($response);
    }

    public function createBooking_post()
    {
        $SqlModel = new SqlModel;
        $BookingModel = new BookingModel;

        // getting form data
        $fname = $this->input->post('fname');
        $lname = $this->input->post('lname');
        $type = $this->input->post('type');
        $vehicle = $this->input->post('vehicle');
        $from = $this->input->post('from');
        $to = $this->input->post('to');

        if(empty($fname) || empty($lname) || empty($type) || empty($vehicle) || empty($from) || empty($to)){
            $err = ''; // defining error
            empty($fname)? $err = "Customer first name cannot be empty" : '';
            empty($lname)? $err = "Customer last name cannot be empty" : '';
            empty($type)? $err = "Vehicle type cannot be empty" : '';
            empty($vehicle)? $err = "Vehicle cannot be empty" : '';
            empty($from)? $err = "From date cannot be empty" : '';
            empty($to)? $err = "To date cannot be empty" : '';

            $response = array(
                "statusCode"=> RestController::HTTP_BAD_REQUEST,
                "error"=> $err
            );
        }else{

            // converting from and to date to yyyy-mm-dd format
            $from = date("Y-m-d", strtotime($from));
            $to = date("Y-m-d", strtotime($to));

            // checking if booking already exist for selected vehicle on selected date
            $bookingExist = $BookingModel->checkIfBookingExist($vehicle, $from, $to);
            if((is_array($bookingExist) && count($bookingExist)) || (is_numeric($bookingExist) && $bookingExist > 0)){
                // booking already exist for selected vehicle on selected date 
                $response = array(
                    "statusCode"=> RestController::HTTP_OK,
                    "error"=> "Booking already exist for selected dates; please select different date range and try again"
                );
            }else{
                // creating a new booking
                $data = array(
                    'vCustFName'=> $fname,
                    'vCustLName' => $lname,
                    'iVTypeID' => $type,
                    'iVehicleID'=> $vehicle,
                    'dFrom' => "$from",
                    'dTo' => "$to",
                    'cStatus'=> 'A'
                );
                $result = $BookingModel->createBooking($data);
                if($result > 0){
                    $response = array(
                        "statusCode"=> RestController::HTTP_OK,
                        "message"=> "Booking created successfully"
                    ); 
                }
                else{
                    $response = array(
                        "statusCode"=> RestController::HTTP_BAD_REQUEST,
                        "message"=> "Something went wrong creating Booking"
                    );
                }
            }
        }
        $this->response($response);
    }
}

?>