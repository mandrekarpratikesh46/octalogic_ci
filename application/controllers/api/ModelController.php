<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/RestController.php';
require APPPATH . 'libraries/Format.php';
use chriskacerguis\RestServer\RestController;

class ModelController extends RestController
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('SqlModel');
    }

    public function index_get($id=0)
    {
        if(empty($id)){
            $SqlModel = new SqlModel;
            $types = $SqlModel->selectAll("vehicle_model");
            $response = array(
                "statusCode"=> RestController::HTTP_OK,
                "message"=> "Vehicle models fetched successfully",
                "data"=> $types
            );
        }else{
            $SqlModel = new SqlModel;
            $cond = array("iVehicleID"=>$id);
            $types = $SqlModel->selectOne("vehicle_model", $cond);
            $response = array(
                "statusCode"=> RestController::HTTP_OK,
                "message"=> "Vehicle model fetched successfully",
                "data"=> $types
            );
        }
        $this->response($response);
    }

    public function createModel_post()
    {
        $SqlModel = new SqlModel;

        // getting form data
        $name = $this->input->post('name');
        $status = $this->input->post('status');
        $type = $this->input->post('type');

        if(empty($name) || empty($status) || empty($type)){
            $err = ''; // defining error
            empty($name)? $err = "Name cannot be empty" : '';
            empty($status)? $err = "Status cannot be empty" : '';
            empty($type)? $err = "Type cannot be empty" : '';

            $response = array(
                "statusCode"=> RestController::HTTP_BAD_REQUEST,
                "error"=> $err
            );
        }else{
            $data = array(
                'iVTypeID'=> $type,
                'vName' => $name,
                'cStatus' => $status
            );
            $result = $SqlModel->insertValue("vehicle_model", $data);
            if($result > 0){
                $response = array(
                    "statusCode"=> RestController::HTTP_OK,
                    "message"=> "Vehicle model created successfully"
                ); 
            }
            else{
                $response = array(
                    "statusCode"=> RestController::HTTP_BAD_REQUEST,
                    "message"=> "Something went wrong creating vehicle model"
                );
            }
        }
        $this->response($response);
    }

    public function updateModel_post($id)
    {
        $SqlModel = new SqlModel;

        // getting form data
        $name = $this->input->post('name');
        $status = $this->input->post('status');
        $type = $this->input->post('type');

        if(empty($name) || empty($status) || empty($type) || empty($id)){
            $err = ''; // defining error
            empty($name)? $err = "Name cannot be empty" : '';
            empty($status)? $err = "Status cannot be empty" : '';
            empty($type)? $err = "Type cannot be empty" : '';
            empty($id)? $err = "ID cannot be empty" : '';

            $response = array(
                "statusCode"=> RestController::HTTP_BAD_REQUEST,
                "error"=> $err
            );
        }else{
            $data = array(
                'iVTypeID'=> $type,
                'vName' => $name,
                'cStatus' => $status
            );
            $cond = array("iVehicleID"=>$id);
            $result = $SqlModel->updateValue("vehicle_model", $cond, $data);
            if($result > 0){
                $response = array(
                    "statusCode"=> RestController::HTTP_OK,
                    "message"=> "Vehicle model updated successfully"
                ); 
            }
            else{
                $response = array(
                    "statusCode"=> RestController::HTTP_BAD_REQUEST,
                    "message"=> "Something went wrong updating vehicle model"
                );
            }
        }
        $this->response($response);
    }

    public function deleteModel_delete($id)
    {
        $SqlModel = new SqlModel;
        if(!empty($id)){
            $cond = array("iVehicleID"=>$id);
            $result = $SqlModel->deleteValue("vehicle_model", $cond);
            if($result > 0){
                $response = array(
                    "statusCode"=> RestController::HTTP_OK,
                    "message"=> "Vehicle model deleted successfully"
                );
            }
            else{
                $response = array(
                    "statusCode"=> RestController::HTTP_BAD_REQUEST,
                    "error"=> "Something went wrong deleting vehicle model"
                );
            }
        }else{
            $response = array(
                "statusCode"=> RestController::HTTP_BAD_REQUEST,
                "error"=> "ID cannot be empty"
            );
        }
        $this->response($response);
    }
}

?>