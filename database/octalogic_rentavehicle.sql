-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 28, 2024 at 06:27 PM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `octalogic_rentavehicle`
--

-- --------------------------------------------------------

--
-- Table structure for table `booking`
--

CREATE TABLE `booking` (
  `iBookingID` int(11) NOT NULL DEFAULT 0,
  `vCode` varchar(255) NOT NULL,
  `vCustFName` varchar(255) NOT NULL,
  `vCustLName` varchar(255) NOT NULL,
  `iVTypeID` int(11) NOT NULL DEFAULT 0,
  `iVehicleID` int(11) NOT NULL DEFAULT 0,
  `dFrom` date NOT NULL,
  `dTo` date NOT NULL,
  `dtCreated` timestamp NOT NULL DEFAULT current_timestamp(),
  `cStatus` char(3) NOT NULL DEFAULT 'A'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `booking`
--

INSERT INTO `booking` (`iBookingID`, `vCode`, `vCustFName`, `vCustLName`, `iVTypeID`, `iVehicleID`, `dFrom`, `dTo`, `dtCreated`, `cStatus`) VALUES
(1, 'OTLC1-89718', 'pratikesh', 'mandrekar', 1, 1, '2024-02-05', '2024-02-10', '2024-01-28 17:15:35', 'A'),
(2, 'OTLC2-45313', 'pratikesh', 'mandrekar', 1, 2, '2024-02-01', '2024-02-12', '2024-01-28 17:24:54', 'A');

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_model`
--

CREATE TABLE `vehicle_model` (
  `iVehicleID` int(11) NOT NULL,
  `iVTypeID` int(11) NOT NULL,
  `vName` varchar(255) NOT NULL,
  `cStatus` char(3) NOT NULL DEFAULT 'A'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `vehicle_model`
--

INSERT INTO `vehicle_model` (`iVehicleID`, `iVTypeID`, `vName`, `cStatus`) VALUES
(1, 1, 'Yamaha R15S', 'A'),
(2, 2, 'Royal Enfield Bullet 350', 'A'),
(3, 3, 'Tata Tiago', 'A'),
(4, 4, 'Hyundai Verna', 'A'),
(5, 5, 'Hyundai Creta', 'A');

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_type`
--

CREATE TABLE `vehicle_type` (
  `iVTypeID` int(11) NOT NULL,
  `iWheelsID` int(11) NOT NULL,
  `vName` varchar(255) DEFAULT NULL,
  `cStatus` char(3) NOT NULL DEFAULT 'A'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `vehicle_type`
--

INSERT INTO `vehicle_type` (`iVTypeID`, `iWheelsID`, `vName`, `cStatus`) VALUES
(1, 1, 'Sports', 'A'),
(2, 1, 'Cruiser', 'A'),
(3, 2, 'Hatchback', 'A'),
(4, 2, 'Sedan', 'A'),
(5, 2, 'SUV', 'A');

-- --------------------------------------------------------

--
-- Table structure for table `wheels`
--

CREATE TABLE `wheels` (
  `iWheelsID` int(11) NOT NULL,
  `vName` varchar(255) NOT NULL,
  `cStatus` char(3) DEFAULT 'A'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `wheels`
--

INSERT INTO `wheels` (`iWheelsID`, `vName`, `cStatus`) VALUES
(1, '2 Wheeler', 'A'),
(2, '4 Wheeler', 'A');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `booking`
--
ALTER TABLE `booking`
  ADD PRIMARY KEY (`iBookingID`);

--
-- Indexes for table `vehicle_model`
--
ALTER TABLE `vehicle_model`
  ADD PRIMARY KEY (`iVehicleID`);

--
-- Indexes for table `vehicle_type`
--
ALTER TABLE `vehicle_type`
  ADD PRIMARY KEY (`iVTypeID`);

--
-- Indexes for table `wheels`
--
ALTER TABLE `wheels`
  ADD PRIMARY KEY (`iWheelsID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `vehicle_model`
--
ALTER TABLE `vehicle_model`
  MODIFY `iVehicleID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `vehicle_type`
--
ALTER TABLE `vehicle_type`
  MODIFY `iVTypeID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `wheels`
--
ALTER TABLE `wheels`
  MODIFY `iWheelsID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
